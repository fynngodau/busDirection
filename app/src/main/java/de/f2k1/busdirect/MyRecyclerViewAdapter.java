package de.f2k1.busdirect;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.typedrecyclerview.SimpleRecyclerViewAdapter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.lang.Math.round;

public class MyRecyclerViewAdapter extends SimpleRecyclerViewAdapter<Bus, MyRecyclerViewAdapter.ViewHolder> {

    private Location location;

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecyclerViewAdapter.ViewHolder holder, Bus item, int position) {

        if (location != null) {
            // Create Location object for bus
            Location to = item.position.asLocation();

            // Display arrow in correct direction
            float bearing = location.bearingTo(to);
            holder.imageView.setRotation(bearing);
            holder.imageView.setVisibility(View.VISIBLE);
        } else {
            // Position in not yet known
            holder.imageView.setVisibility(View.GONE);
        }

        if (item.position.getDistance() > -1) {
            holder.tvDistance.setText((round(item.position.getDistance() * 100)) / 100 + " m");
            holder.tvDistance.setVisibility(View.VISIBLE);
        } else {
            // Position is not yet known
            holder.tvDistance.setVisibility(View.GONE);
        }

        String text = item.display.getLinie() + " " + item.display.getZiel();

        /*
        if(!busDetails.isStadtbus(mData.get(i).getId())) {
            holder.tvBusName.setTypeface(null, Typeface.ITALIC);
        }*/
        holder.tvBusName.setText(text);
        holder.tvSpeed.setText(item.position.getSpeed() + " km/h");
        holder.tvBusId.setText(item.getId());

        holder.busId = item.getId();
    }

    public void setData(List<Bus> busses) {

        content.clear();
        content.addAll(busses);

        onRelativeLocationChange();
    }

    /**
     * Called when either location or busses update
     */
    private void onRelativeLocationChange() {

        if (location != null) {

            // Attach distance to positions
            for (Bus bus : content) {
                float distance = MainActivity.getMeters(bus.position.getLon(), bus.position.getLat(), location.getLongitude(), location.getLatitude());
                bus.position.setDistance(distance);
            }

            // Sort by distance
            Collections.sort(content, new Comparator<Bus>() {
                @Override
                public int compare(Bus bus, Bus bus1) {
                    return (int) (bus.position.getDistance() * 10000 - bus1.position.getDistance() * 10000);
                }
            });
        }

        notifyDataSetChanged();
    }

    public void onLocationChanged(Location location) {
        this.location = location;
        onRelativeLocationChange();
    }

    // stores and recycles views as they are scrolled off screen
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvBusName, tvDistance, tvSpeed, tvBusId;
        ImageView imageView;

        String busId;

        ViewHolder(View itemView) {
            super(itemView);
            tvBusName = itemView.findViewById(R.id.tvBusName);
            tvDistance = itemView.findViewById(R.id.tvDistance);
            imageView = itemView.findViewById(R.id.imageView);
            tvSpeed = itemView.findViewById(R.id.tvSpeed);
            tvBusId = itemView.findViewById(R.id.tvBusId);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String id = busId;
            String betreiber = BusDetails.getBusBetrieberById(id);
            String nummer = BusDetails.getBusnumberById(id);
            String kennzeichen = BusDetails.getKennzeichenById(id);
            String fahrzeug = BusDetails.getFahrzeugById(id);
            Toast.makeText(view.getContext(), "Betreiber:" + betreiber + "\n" + "Fahrzeug:" + fahrzeug + "\n" + "Nummer:" + nummer + "\n" + "Kennzeichen:" + kennzeichen + "\n" + "GPSaugeID: " + id, Toast.LENGTH_SHORT).show();
        }
    }
}