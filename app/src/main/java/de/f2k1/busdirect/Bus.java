package de.f2k1.busdirect;

import android.location.Location;
import androidx.annotation.Nullable;

public class Bus {
    final Position position;
    final Anzeige display;

    public Bus(Position position, Anzeige display) {
        this.position = position;
        this.display = display;
    }

    public String getId() {
        return position.getId();
    }

    public static class Position {
        private final String id;
        private final double lon;
        private final double lat;
        private final int dir;
        private final double speed;
        private final long timestamp;
        private double meters = -1;

        Position(String pId, double pLon, double pLat, int pDir, double pSpeed, long pTimestamp) {
            id = pId;
            lon = pLon;
            lat = pLat;
            dir = pDir;
            speed = pSpeed;
            timestamp = pTimestamp;
        }

        String getId() {
            return id;
        }

        double getLon() {
            return lon;
        }

        double getLat() {
            return lat;
        }

        int getDir() {
            return dir;
        }

        double getSpeed() {
            return speed;
        }

        long getTimestamp() {
            return timestamp;
        }

        void setDistance(double pMeters) {
            meters = pMeters;
        }

        public double getDistance() {
            return meters;
        }

        public Location asLocation() {
            Location location = new Location("");
            location.setLatitude(getLat());
            location.setLongitude(getLon());
            return location;
        }

    }

    public static class Anzeige {
        private final String id;
        private final String linie;
        private final String ziel;

        Anzeige(String pId, @Nullable String pLinie, @Nullable String pZiel) {
            id = pId;
            linie = pLinie;
            ziel = pZiel;
        }

        public String getId() {
            return id;
        }

        public String getLinie() {
            if (linie == null) return "";
            else return linie.trim();
        }

        public String getZiel() {
            if (ziel == null) return "";
            else return ziel.replaceAll("~", "ẞ").trim();
        }
    }

}



