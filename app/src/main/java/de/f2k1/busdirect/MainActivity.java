package de.f2k1.busdirect;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.List;

import de.f2k1.busdirect.fragments.ListFragment;
import godau.fynn.librariesdirect.AboutLibrariesActivity;
import godau.fynn.librariesdirect.model.Library;
import godau.fynn.librariesdirect.model.License;


public class MainActivity extends AppCompatActivity implements SleepGetBusses.Callback {
    private SleepGetBusses getBusses = new SleepGetBusses();
    private final LocationDistributor locationDistributor = new LocationDistributor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment, new ListFragment())
                .commit();

        getBusses.start();
        getBusses.attach(this);

        requestLocationUpdates();
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        super.onAttachFragment(fragment);

        // Attach fragment to thread
        if (fragment instanceof SleepGetBusses.Callback) {
            getBusses.attach((SleepGetBusses.Callback) fragment);
        }

        // Attach fragment to location distributor.
        if (fragment instanceof LocationListener) {
            locationDistributor.attach((LocationListener) fragment);
        }

        /* Why is there no onDetachFragment(Fragment)?
         * Note that due to that, the fragment is not detached from the thread.
         * Thus, the fragment needs to be prepared not to have a valid context
         * in its implementations of the SleepGetBusses.Callback interface.
         * Same applies to location distributor.
         */
    }

    @Override
    protected void onPause() {
        super.onPause();
        getBusses.interrupt();
    }

    protected void onResume() {
        super.onResume();
        if (!getBusses.isAlive()) {
            getBusses = new SleepGetBusses(getBusses);
            getBusses.start();
        }
    }

    @Override
    public void onBusUpdate(List<Bus> busses) {
        findViewById(R.id.mainactivityhint).setVisibility(View.GONE);
    }

    @Override
    public void onNetworkError() {
        // TODO handle network error here
        Log.e(MainActivity.class.getSimpleName(), "A network error occurred");
        findViewById(R.id.mainactivityhint).setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.about:

                Intent intent = new AboutLibrariesActivity.IntentBuilder(this)
                        .setHeaderText(getString(R.string.about_header))
                        .setLibraries(new Library[]{
                                new Library("OkHttp", License.APACHE_20_LICENSE, null, "Square, Inc.", "https://square.github.io/okhttp/"),
                                new Library("TypedRecyclerView", License.CC0_LICENSE, null, "Fynn Godau", "https://codeberg.org/fynngodau/TypedRecyclerView"),
                                new Library("librariesDirect", License.CC0_LICENSE, null, "Fynn Godau", "https://codeberg.org/fynngodau/librariesDirect"),
                        })
                        .build();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void requestLocationUpdates() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            // Permission not granted; request
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)){
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        } else {
            // Permission already granted
            LocationManager locationManager;
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates("fused", // LocationManager.FUSED_PROVIDER
                    0, 0, locationDistributor);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "GPS Permission Granted", Toast.LENGTH_SHORT).show();

                        // Request location updates after permission has been granted
                        requestLocationUpdates();
                    }
                } else {
                    Toast.makeText(this, "GPS Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    static float getMeters(double lon1, double lat1, double lon2, double lat2) {
        float[] result =  new float[1];
        Location.distanceBetween(lat1, lon1, lat2, lon2, result);
        return result[0];
    }
}
