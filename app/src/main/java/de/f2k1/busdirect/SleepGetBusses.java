package de.f2k1.busdirect;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class SleepGetBusses extends Thread {

    private final List<Callback> callbacks;

    private final Handler mainThreadHandler = new Handler(Looper.getMainLooper());
    private final OkHttpClient okHttpClient = new OkHttpClient();

    public SleepGetBusses() {
        callbacks = new ArrayList<>();
    }

    /**
     * Copies callbacks from other thread
     */
    public SleepGetBusses(SleepGetBusses from) {
        this.callbacks = from.callbacks;
    }

    public void run() {

        try {
            while (true) {

                try {
                    List<Bus.Anzeige> anzeigen = getAnzeigen();
                    List<Bus.Position> busPositions = getBusses();
                    List<Bus> busses = merge(anzeigen, busPositions);

                    notifyCallbacks(busses);

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    notifyCallbacksNetworkError();
                }

                sleep(5000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private List<Bus.Anzeige> getAnzeigen() throws IOException, JSONException {

        List<Bus.Anzeige> anzeigen = new ArrayList<>();

        Request request = new Request.Builder()
                .url("https://stadtwerke.24stundenonline.de/request.php?action=getfmsdata&fields=stadtwerke_sw_aussenanzeige%2Cstadtwerke_sw_liniennummer%2Cstadtwerke_sw_kursnummer&deviceid=217796%2C217797%2C217790%2C217765%2C217800%2C217808%2C217810%2C217777%2C217802%2C217778%2C217809%2C217813%2C217804%2C217772%2C217776%2C217771%2C217783%2C217801%2C217795%2C217811%2C217779%2C217775%2C217787%2C217814%2C217764%2C217799%2C217781%2C217763%2C217782%2C217786%2C217815%2C217806%2C217816%2C217803%2C217792%2C217762%2C217791%2C217769%2C217770%2C217805%2C217812%2C217767%2C217794%2C217774%2C217807%2C217793%2C217789%2C217773%2C217766%2C217780%2C217788%2C217798%2C217768%2C217784%2C217785&_=1586519801628")
                .build();
        String res = okHttpClient.newCall(request).execute()
                .body().string();

        JSONObject reader = new JSONObject(res);
        JSONObject c = reader.getJSONObject("fields");
        Iterator<String> keys = c.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            JSONObject object = c.getJSONObject(key);

            String aussenanzeige = null;
            if (object.has("stadtwerke_sw_aussenanzeige")) {
                aussenanzeige = object.getString("stadtwerke_sw_aussenanzeige");
            }

            String liniennummer = null;
            if (object.has("stadtwerke_sw_liniennummer")) {
                liniennummer = object.getString("stadtwerke_sw_liniennummer");
            }
            anzeigen.add(new Bus.Anzeige(key, liniennummer, aussenanzeige));
        }

        return anzeigen;

    }

    private List<Bus.Position> getBusses() throws JSONException, IOException {

        List<Bus.Position> l = new ArrayList<>();

        Request request = new Request.Builder()
                .url("https://stadtwerke.24stundenonline.de/request.php?action=getpositions")
                .build();
        String res = okHttpClient.newCall(request).execute()
                .body().string();
        Log.d("MA", res);

        JSONArray reader = new JSONArray(res);
        Log.d("MA", String.valueOf(reader.length()));

        for(int i = 0; i < reader.length(); i++) {
            JSONObject c = reader.getJSONObject(i);
            String id = c.getString("id");
            double lon = c.getDouble("WE");
            double lat = c.getDouble("NS");
            int dir = c.getInt("dir");
            double speed = c.getDouble("speed");
            long timestamp = c.getLong("unix_ts");

            l.add(new Bus.Position(id, lon, lat, dir, speed, timestamp));
        }



        return l;
    }

    private List<Bus> merge(List<Bus.Anzeige> displays, List<Bus.Position> positions) {
        List<Bus> busses = new ArrayList<>();

        positions:
        for (Bus.Position position : positions) {

            // Find matching display
            for (Bus.Anzeige display : displays) {
                if (position.getId().equals(display.getId())) {
                    busses.add(new Bus(position, display));
                    continue positions;
                }
            }
        }

        return busses;
    }

    /**
     * Attach callback to thread. Callback will run on UI thread whenever the thread has received
     * new events.
     */
    public void attach(Callback handler) {
        callbacks.add(handler);
    }

    public void detach(Callback handler) {
        callbacks.remove(handler);
    }

    private void notifyCallbacks(final List<Bus> busses) {

        for (final Callback callback : callbacks) {
            mainThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    callback.onBusUpdate(busses);
                }
            });
        }
    }

    private void notifyCallbacksNetworkError() {

        for (final Callback callback : callbacks) {
            mainThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    callback.onNetworkError();
                }
            });
        }
    }


    public interface Callback {

        /**
         * Called when new busses are downloaded
         *
         * @param busses A list of busses sorted by distance
         */
        void onBusUpdate(List<Bus> busses);

        /**
         * Called when a network error occurs.
         */
        void onNetworkError();
    }
}
