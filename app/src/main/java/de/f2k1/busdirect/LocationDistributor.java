package de.f2k1.busdirect;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class LocationDistributor implements LocationListener {

    private final List<LocationListener> distributeList = new ArrayList<>();

    public void attach(LocationListener locationListener) {
        distributeList.add(locationListener);
    }

    @Override
    public void onLocationChanged(Location location) {
        for (LocationListener listener : distributeList) {
            listener.onLocationChanged(location);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        for (LocationListener listener : distributeList) {
            listener.onStatusChanged(provider, status, extras);
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        for (LocationListener listener : distributeList) {
            listener.onProviderEnabled(provider);
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        for (LocationListener listener : distributeList) {
            listener.onProviderDisabled(provider);
        }
    }
}
